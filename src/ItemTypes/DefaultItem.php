<?php

namespace Shop\ItemTypes;

use Shop\Item;

class DefaultItem implements ItemTypeInterface
{
    public function updateItem(Item $item): void
    {
        if ($item->quality > self::MIN_QUALITY) {
            $item->quality -= 1;
        }

        $item->sell_in -= 1;

        if ($item->sell_in < self::MIN_SELL_IN && $item->quality > self::MIN_QUALITY) {
            $item->quality -= 1;
        }
    }
}