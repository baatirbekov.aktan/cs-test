<?php

namespace Shop\ItemTypes;

use Shop\Item;

interface ItemTypeInterface
{
    public const MAX_QUALITY = 50;

    public const MIN_QUALITY = 0;

    public const MIN_SELL_IN = 0;

    public function updateItem(Item $item): void;
}