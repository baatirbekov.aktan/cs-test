<?php

declare(strict_types=1);

namespace Shop;

use Shop\ItemTypes\DefaultItem;

final class Shop
{
    /**
     * @var Item[]
     */
    private array $items;

    /**
     * @param array $items
     */
    public function __construct(array $items)
    {
        $this->items = $items;
    }

    /**
     * @return void
     */
    public function updateQuality(): void
    {
        foreach ($this->items as $item) {

            $className = 'Shop\ItemTypes' . '\\' . str_replace(' ', '', ucwords($item->name));

            if (class_exists($className)) {
                $object = new $className();
            } else {
                $object = new DefaultItem();
            }

            $object->updateItem($item);
        }
    }
}
