stop:
	docker compose stop
shell:
	docker compose exec app sh
start:
	docker compose up --detach
destroy:
	docker compose down --remove-orphans --volumes
build:
	docker compose build --pull
composer-install:
	docker compose exec app composer install
test:
	docker compose exec app ./vendor/bin/phpunit
init: destroy build start composer-install test
